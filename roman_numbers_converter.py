import unittest

def ten_up_to(x):
    return 10 ** x


def solution(n):
    bloody_romans = {1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M' }
    
    if n < 0 or n  > 3999:
        return False

    num = str(n)
    power = len(num) - 1
    xx = ''
    for num_part in num:
        if num_part == '0':
            power -= 1
        if num_part in ['1', '2', '3']:
            xx += bloody_romans[1 * ten_up_to(power)] * int(num_part)
            power -= 1
        if num_part == '4': 
            xx += bloody_romans[1 * ten_up_to(power)] + bloody_romans[5 * ten_up_to(power)]
            power -= 1
        if num_part == '5':
            xx += bloody_romans[5 * ten_up_to(power)]
            power -= 1
        if num_part in ['6', '7', '8']:
            xx += bloody_romans[5 * ten_up_to(power)] + bloody_romans[1 * ten_up_to(power)] * (int(num_part) % 5)
            power -= 1
        if num_part == '9':
            xx += bloody_romans[1 * ten_up_to(power)] + bloody_romans[10 * ten_up_to(power)]
            power -= 1
    return xx


class TestRomanConversion(unittest.TestCase):
    
    def test_up_to_eight(self):
        self.assertEqual('I', solution(1))
        self.assertEqual('II', solution(2))
        self.assertEqual('III', solution(3))
        self.assertEqual('IV', solution(4))
        self.assertEqual('V', solution(5))
        self.assertEqual('VI', solution(6))
        self.assertEqual('VII', solution(7))
        self.assertEqual('VIII', solution(8))
        
    def test_from_nine_to_eighteen(self):
        self.assertEqual('IX', solution(9))
        self.assertEqual('X', solution(10))
        self.assertEqual('XI', solution(11))
        self.assertEqual('XII', solution(12))
        self.assertEqual('XIII', solution(13))
        self.assertEqual('XIV', solution(14))
        self.assertEqual('XV', solution(15))
        self.assertEqual('XVI', solution(16))
        self.assertEqual('XVII', solution(17))
        self.assertEqual('XVIII', solution(18))
    
    def test_from_eighteen(self):
        self.assertEqual('XIX', solution(19))
        self.assertEqual('XX', solution(20))
        self.assertEqual('XXXI', solution(31))
        self.assertEqual('XL', solution(40))
        self.assertEqual('L', solution(50))
        self.assertEqual('LXIV', solution(64))
        self.assertEqual('CL', solution(150))

    def test_from_wiki_wo_zeros(self):
        self.assertEqual('XXXIX', solution(39))
        self.assertEqual('CCXLVI', solution(246))
        self.assertEqual('DCCLXXXIX', solution(789))
        self.assertEqual('MMCDXXI', solution(2421))
        
    def test_from_wiki_zeros(self):
        self.assertEqual('CLX', solution(160))
        self.assertEqual('CCVII', solution(207))
        self.assertEqual('MIX', solution(1009))
        self.assertEqual('MLXVI', solution(1066))

    def test_from_wiki_large_nums(self):
        self.assertEqual('MDCCLXXVI', solution(1776))
        self.assertEqual('MCMLIV', solution(1954))
        self.assertEqual('MMXIV', solution(2014))
        self.assertEqual('MMXX', solution(2020))
        
    def test_out_of_range(self):
        self.assertFalse(solution(4000))
        self.assertFalse(solution(-1))
        self.assertFalse(solution(0))

if __name__ == '__main__':
    unittest.main()
