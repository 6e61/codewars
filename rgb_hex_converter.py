import unittest

def rgb_to_hex(r, g, b):
    new_hex = ''
    for i in [r, g, b]:
        if i in range(0,256):
            new_hex += f'{i:02X}'
        elif i < 0:
            new_hex += f'{0:02X}'
        elif i > 255:
            new_hex += f'{255:02X}'
            
    return new_hex


class TestRgbToHex(unittest.TestCase):

    def test_in_range(self):
        self.assertEqual('FFFFFF', rgb_to_hex(255, 255, 255))
    
    def test_out_of_range(self):
        self.assertEqual('FFFFFF', rgb_to_hex(267,256,12365))
        self.assertEqual('FF000F', rgb_to_hex(293, -4, 15))

if __name__ == '__main__':
    unittest.main()
