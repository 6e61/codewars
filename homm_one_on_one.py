from math import ceil



def attack(m1, m2):
    dmg = m1.get('number') * m1.get('damage')
    hp_left = m2.get('hitpoints') * m2.get('number') - dmg
    print(f' # hp left: {hp_left}')
    return ceil(hp_left / m2.get('hitpoints'))


if __name__ == '__main__':
    mon1 = {"type": "Roc",     "hitpoints": 40, "number": 6, "damage" : 8 }
    mon2 = {"type": "Unicorn", "hitpoints": 40, "number": 4, "damage" : 13}
    
    units_left = min(mon1['number'], mon2['number'])
    while units_left:
        print(f'\n--> units left on bf: {units_left}\n'
                f'attacker: {mon1}\n'
                f'defender: {mon2}')
        units_left = attack(mon1, mon2)
        print(f' $ units left after att vs def: {units_left}')
        if not units_left:
            break    
        mon2.update(number = units_left)
        units_left = attack(mon2, mon1)
        print(f' $ units left after def vs att: {units_left}')
        mon1.update(number = units_left)
