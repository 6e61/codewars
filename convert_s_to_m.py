import unittest

def format_duration(seconds):
    if seconds <= 0:
        return('now')

class testFD(unittest.TestCase):
    def test_format_now(self):
        self.assertEqual('now', format_duration(0))
        self.assertEqual('now', format_duration(-1))



if __name__ == '__main__':
    unittest.main()
