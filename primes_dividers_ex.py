import unittest
"""
find all primes for items in list
sort them (as set)
sum all items from input list for which this prime is a factor of list

* remember all primes for each item in list
* make new list with not repetable primes
* find and sum remembered items when searching primes

"""

def less_or_eq(lst):
    return max([abs(i) for i in lst])


def is_prime(num):
    if num <= 1:
        return False
    div = 2
    num_x = round(num ** 0.5)  # cut half of tests
    while div <= num_x: 
        if not num % div:
            return False
        div += 1
    return True


def find_primes(lst):
    primes_list = []
    loe = less_or_eq(lst)
    div = 2
    while div <= loe:
        if is_prime(div):
            primes_list.append(div)
        div += 1
    print(f'\n >> primes list: {primes_list}\n')
    return primes_list



def sum_for_list(lst):
    primes_list = find_primes(lst)
    output_list = []
    for item in lst:
        for prime in primes_list:
            added = False
            if not item % prime:  # 0=True, !0=False / if divisor
                if output_list:  # list has elements
                    print(f'prime: {prime} and item {item} to list {output_list}\n')
                    for out_lst_item in output_list:
                        if prime in out_lst_item:
                            out_lst_item[1] += item
                            added = True
                            print(f'-------sum for prime {prime}, {item}  on with sum {out_lst_item}\n')
                    if not added:
                        output_list.append([prime, item])
                        print(f' ++ added [] to out_lst {output_list}\n')
                else:
                    output_list.append([prime, item])
            if prime > abs(item):
                break
    return sorted(output_list)


"""
if not output_list:  # list empty
                    print(f'empty prime: {prime} and item: {item}\n')
                    output_list.append([prime, item])
                else:
                    print(f'outpt lst: {output_list}\n')
                    print(f'prime: {prime} and item {item}\n')
                    for out_lst_item  in output_list:
                        if prime in out_lst_item:
                            out_lst_item[1] += item
                            print(f'------- prime {prime} on list {out_lst_item}\n')
                    
                    output_list.append([prime, item])
"""
 




class TestSumForLst(unittest.TestCase):
    def test_full_task(self):
        self.assertEqual([[2, 12], [3, 27], [5,15]], sum_for_list([12, 15]))
        self.assertEqual([[2, 54], [3, 45], [5, 0], [7, 21]], sum_for_list([15, 21, 24, 30, -45]))
"""
    def test_is_prime(self):
        self.assertTrue(is_prime(2))
        self.assertTrue(is_prime(3))
        self.assertTrue(is_prime(11))
        self.assertFalse(is_prime(1))
        self.assertFalse(is_prime(6))

    def test_find_primes(self):
        self.assertEqual([2, 3, 5, 7, 11], find_primes([4, 5, -11]))
        self.assertEqual([2, 3, 5, 7, 11], find_primes([4, 5, 12]))
"""

if __name__ == '__main__':
    unittest.main()
